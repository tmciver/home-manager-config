Config { overrideRedirect = False
       , font     = "xft:iosevka-9"
       , bgColor  = "#5f5f5f"
       , fgColor  = "#f8f8f2"
       , position = TopW L 90
       , commands = [ Run Weather "EGPF"
                        [ "--template", "<weather> <tempF>°F"
                        , "-L", "32"
                        , "-H", "80"
                        , "--low"   , "lightblue"
                        , "--normal", "#f8f8f2"
                        , "--high"  , "red"
                        ] 36000
                    , Run Cpu
                        [ "-L", "3"
                        , "-H", "50"
                        , "--high"  , "red"
                        , "--normal", "green"
                        ] 10
                    , Run Battery
                        [ "--template" , "Bat: <acstatus>"
                        , "--L" , "15"
                        , "--H" , "75"
                        , "--low"      , "darkred"
                        , "--normal"   , "darkorange"
                        , "--high"     , "#1ABC9C"
                        , "--"
                        -- battery specific options
                        -- discharging status
                        , "-o"	, "<left>% (<timeleft>)"
                        -- AC "on" status
                        , "-O"	, "<fc=#dAA520>Charging</fc>"
                        -- charged status
                        , "-i"	, "<fc=#1ABC9C>Charged</fc>"
                        ] 50
                    , Run Alsa "default" "Master"
                        [ "--template", "<volumestatus>"
                        , "--suffix"  , "True"
                        , "--"
                        , "--on", ""
                        ]
                    , Run Memory ["--template", "Mem: <usedratio>%"] 10
                    , Run Swap [] 10
                    , Run Date "%a %Y-%m-%d <fc=#8be9fd>%I:%M %p</fc>" "date" 10
                    , Run StdinReader
                    ]
       , sepChar  = "%"
       , alignSep = "}{"
       , template = "%StdinReader% }{ %battery% | %alsa:default:Master% | %cpu% | %memory% * %swap% | %EGPF% | %date% "
       }
