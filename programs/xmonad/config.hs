import Control.Monad (void)
import XMonad
import XMonad.Util.EZConfig (additionalKeysP, additionalKeys)
import XMonad.Util.Run (spawnPipe, hPutStrLn)
import XMonad.Hooks.ManageDocks (docks, avoidStruts, ToggleStruts(ToggleStruts))
import XMonad.Hooks.DynamicLog (dynamicLogWithPP, ppOutput)
import XMonad.Hooks.Rescreen (rescreenHook, randrChangeHook)
import XMonad.Actions.Volume (lowerVolume, raiseVolume, toggleMute)
import XMonad.Layout.Spacing (spacingRaw, Spacing, Border(..), toggleWindowSpacingEnabled, toggleScreenSpacingEnabled, toggleSmartSpacing)
import XMonad.Layout.LayoutModifier (ModifiedLayout)
import XMonad.Hooks.SetWMName (setWMName)

myModMask = mod4Mask

myKeysP :: [(String, X ())]
myKeysP =
  [ ("<XF86AudioLowerVolume>", void $ lowerVolume 5)
  , ("<XF86AudioRaiseVolume>", void $ raiseVolume 5)
  , ("<XF86AudioMute>", void toggleMute)
  , ("<Print>", spawn "scrot -s")
  ]

myKeys :: [((KeyMask, KeySym), X ())]
myKeys =
  [ ((myModMask .|. shiftMask, xK_s), toggleWindowSpacingEnabled)
  , ((myModMask, xK_s), toggleScreenSpacingEnabled)
  , ((myModMask, xK_o), toggleSmartSpacing)
  , ((myModMask, xK_b), sendMessage ToggleStruts)
  ]

mySpacing :: Integer -> l a -> ModifiedLayout Spacing l a
mySpacing i = spacingRaw True (Border i 0 i 0) True (Border 0 i 0 i) True

myLayout = avoidStruts . mySpacing 10 . layoutHook $ def

myRandrChangeHook :: X ()
myRandrChangeHook = spawn "autorandr --change"

rescreenCfg = def { randrChangeHook = myRandrChangeHook }

main = do
  h <- spawnPipe "xmobar -x 0 /home/tim/.config/xmobar/.xmobarrc"
  xmonad
    $ rescreenHook rescreenCfg
    $ docks def
    { modMask = myModMask
    , terminal = "alacritty"
    , layoutHook = myLayout
    , logHook = dynamicLogWithPP $ def { ppOutput = hPutStrLn h }
    , startupHook = setWMName "LG3D"
    }
    `additionalKeysP` myKeysP
    `additionalKeys` myKeys
