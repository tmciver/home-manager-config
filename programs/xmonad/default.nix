{ ... }:

let
  startNitrogen = "nitrogen --restore &";
  startTrayer = ''
    trayer --monitor primary --edge top --align right --SetDockType true --SetPartialStrut true \
    --expand true --width 10 --transparent true --alpha 0xff --tint 0xff5f5f5f --height 19 &
    '';
  startNM = "nm-applet --sm-disable &";
  startBlueman = "blueman-applet &";
in
{
  xsession = {
    enable = true;

    initExtra = startNitrogen + startTrayer + startNM + startBlueman;

    windowManager.xmonad = {
      enable = true;
      enableContribAndExtras = true;
      extraPackages = hp: [
        hp.dbus
        hp.monad-logger
        hp.xmonad-contrib
        hp.xmonad-extras
      ];
      config = ./config.hs;
    };
  };
}

