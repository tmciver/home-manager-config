{ ... }:

{
  programs.urxvt = {
    enable = true;
    transparent = true;
    shading = 175;
  };
}
