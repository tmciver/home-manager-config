{ ... }:

{
  programs.alacritty = {
    enable = true;
    settings = {
      window = {
        colors = {
          primary = {
            background = "#040404";
            foreground = "#c5c8c6";
          };
        };
        decorations = "full";
        opacity = 0.8;
      };
    };
  };
}
