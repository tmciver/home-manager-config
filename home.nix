{ config, pkgs, ... }:

{
  # Home Manager needs a bit of information about you and the
  # paths it should manage.
  home.username = "tim";
  home.homeDirectory = "/home/tim";

  # This value determines the Home Manager release that your
  # configuration is compatible with. This helps avoid breakage
  # when a new Home Manager release introduces backwards
  # incompatible changes.
  #
  # You can update Home Manager without changing this value. See
  # the Home Manager release notes for a list of state version
  # changes in each release.
  home.stateVersion = "21.11";

  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;

  home.packages = with pkgs; [
    autorandr
    trayer
    networkmanagerapplet
    okular
    nitrogen
    protege-distribution
    scrot
    ledger-live-desktop
  ];

  imports = [
    # programs
    ./programs/xmonad/default.nix
    ./programs/xmobar/default.nix
    ./programs/urxvt/default.nix
    ./programs/alacritty/default.nix

    # services
    ./services/picom/default.nix
  ];

  programs.bat.enable = true;
  services.network-manager-applet.enable = true;
}
